// Prototype YAML deserialisation filter for billion laughs and billion hashes


#include <stdio.h>
#include <yaml.h>
#include <assert.h>
#include <stack> 
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#define DEBUG 0

using namespace std; 

unordered_map<int, long long> visited; // anchor -> object (seq or map)

long long dfs(int s, int t,  vector<vector<int>>  adj) {
  if (s == t) {
    return 1LL;
  }
  else {
    if (visited[s] == -1) {
      visited[s] = 0LL;
      long long sum = 0LL;
      for(int i = 0; i < adj[s].size(); i++) {
        sum = sum + dfs(adj[s].at(i), t, adj);
      }
      visited[s] = sum;
    }
  }
  return visited[s];
}



int main(int argc, char** argv)
{

  vector<int> leaves;
  stack<int> stack;
  vector<tuple<int, int>> edges;
  vector<tuple<int, int>>::iterator it;
  vector<int>::iterator leaves_it;
  
  unordered_map<string, int> anchors; // anchor -> object (seq or map)
  string anchor = ""; // last recorded anchor
  int current, older;
  string tmp;
  int count = 0; 
  FILE *file;
  yaml_parser_t parser;
  yaml_token_t  token;
  
  file = fopen(argv[1], "rb");
  assert(file);

  // Initialize parser and set input file
  if(!yaml_parser_initialize(&parser))
    fputs("Failed to initialize parser!\n", stderr);

  yaml_parser_set_input_file(&parser, file);

  do {
    yaml_parser_scan(&parser, &token);
    switch(token.type)
    {
    case YAML_FLOW_SEQUENCE_START_TOKEN: // TODO: potential pairing bug if flow/block styles are interspersed?
    case YAML_BLOCK_SEQUENCE_START_TOKEN:
      current = count;
      count = count + 1;
      if (anchor == "")
        ;
      else {
        anchors[string(anchor)] =  current;
        anchor = "";
      }
      if (!stack.empty()) {
        edges.push_back(make_tuple(stack.top(), current));
        if(DEBUG)
          cout << stack.top() << " -> " << current << endl;
      }
      stack.push(current);      
      break;
    case YAML_FLOW_SEQUENCE_END_TOKEN:      
    case YAML_BLOCK_END_TOKEN:
      stack.pop();
      break;
    case YAML_BLOCK_MAPPING_START_TOKEN:
      current = count;
      count = count + 1;
      if (anchor == "")
        ;
      else {
        anchors[string(anchor)] = current;
        anchor = "";
      }
      if (!stack.empty()) {
        if (DEBUG)
          cout << stack.top() << " -> " << current << endl;
        edges.push_back(make_tuple(stack.top(), current));        
      }
      stack.push(current);      
      break;
    case YAML_ALIAS_TOKEN:
      tmp = "";
      tmp.append(reinterpret_cast<const char*>(token.data.alias.value));      
      older = anchors[tmp];
      edges.push_back(make_tuple(stack.top(), older));
      if (DEBUG)
        cout << stack.top() << " -> " << older << endl;      
      break;
    case YAML_ANCHOR_TOKEN:
      tmp = "";
      tmp.append(reinterpret_cast<const char*>(token.data.anchor.value));           
      anchor = tmp;
      break;
    case YAML_SCALAR_TOKEN:
      break;
    case YAML_BLOCK_ENTRY_TOKEN:
      break;
    case YAML_STREAM_START_TOKEN:
      break;
    case YAML_STREAM_END_TOKEN:
      break;
    case YAML_KEY_TOKEN:
      break;
    case YAML_VALUE_TOKEN:
      break;      
    default:
      printf("Unhandled token type: %d\n", token.type);
    }
    if(token.type != YAML_STREAM_END_TOKEN)
      yaml_token_delete(&token);
  } while(token.type != YAML_STREAM_END_TOKEN);
  yaml_token_delete(&token);

  count;
  vector<vector<int>> adj(count, vector<int>(0));

  for(int i = 0; i < count; i++) {
    visited[i] = -1;
  }
  
  for(it = edges.begin(); it != edges.end(); it++)    {
    tuple<int, int> t = *it;
    int fromV = get<0>(t);
    int toV = get<1>(t);
    adj[fromV].push_back(toV);
  }

  if (DEBUG) {
    cout << endl;    
    for(int i = 0; i < count; i++) {
      for(int j=0; j < adj[i].size(); j++)
        cout << i << " -> " << adj[i].at(j) << endl;    
    }
  }

  cout << "Total vertices: " << count << endl;
  cout << "Total edges: " << edges.size() << endl;
  cout << "Leaves: ";
  for(int i = 0; i < count; i++) {
    if (adj[i].empty()) {
      if (!leaves.empty())
        cout << ", ";
      
      cout << i ;
      leaves.push_back(i);
    }
  }
  if (leaves.empty())
    cout << "None" << endl;
  else
    cout << "\nTotal leaves: " << leaves.size() << endl;

  long long pathCount = 0;
  
  for(leaves_it = leaves.begin(); leaves_it != leaves.end(); leaves_it++)    {
    int t = *leaves_it;
    pathCount = pathCount + dfs(0, t, adj);
  }
  cout << "Path count: " << pathCount << endl;
  yaml_parser_delete(&parser);
  fclose(file);
  return 0;
}
