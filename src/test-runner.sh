#!/bin/bash
LOG_FILE=/out/log.txt
cd /src/
libraries=`cat libraries.txt`
payloads=`cat testvectors.txt`
IFS=$'\n'
for item in $libraries;
do
echo $item | tee -a $LOG_FILE    
    for payload in $payloads;
    do
        cmd="timeout 2m $item $payload"
        eval $cmd
        echo $payload $? | tee -a $LOG_FILE        
  done
done
