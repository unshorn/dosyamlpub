require 'psych'
require 'objspace'
data = File.read(ARGV[0])
struc = Psych.safe_load data, aliases: true
puts ObjectSpace::memsize_of(struc)
