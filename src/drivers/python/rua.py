from ruamel.yaml import YAML
import sys

yaml = YAML(typ='safe')
with open(sys.argv[1], 'r') as stream:
  yaml.load(stream)
