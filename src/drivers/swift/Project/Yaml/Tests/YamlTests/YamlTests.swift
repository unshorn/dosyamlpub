import XCTest
@testable import Yaml

final class YamlTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Yaml().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
