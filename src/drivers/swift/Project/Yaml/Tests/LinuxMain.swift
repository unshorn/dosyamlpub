import XCTest

import YamlTests

var tests = [XCTestCaseEntry]()
tests += YamlTests.allTests()
XCTMain(tests)