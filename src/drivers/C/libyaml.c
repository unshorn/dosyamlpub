#include <stdio.h>
#include <yaml.h>
#include <assert.h>

int main(int argc, const char *argv[])
{
  FILE *file;
  yaml_parser_t parser;
  yaml_document_t document;
  yaml_node_t *node;
  int i = 1;

  file = fopen(argv[1], "rb");
  assert(file);

  assert(yaml_parser_initialize(&parser));

  yaml_parser_set_input_file(&parser, file);

  if (!yaml_parser_load(&parser, &document)) {
    goto done;
  }

  while(1) {
    node = yaml_document_get_node(&document, i);
    if(!node) break;
    fprintf(stderr, "Node [%d]: %d\n", i++, node->type);
  }
  yaml_document_delete(&document);


done:
  yaml_parser_delete(&parser);
  assert(!fclose(file));

  return 0;
}
