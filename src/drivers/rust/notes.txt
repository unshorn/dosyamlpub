kernel is killing the process 
core dumps are configured via ulimits
if the kernel decides to kill a process, the process has no indication or control of that happening
you can't install a SIG_KILL handler so the process just terminates
maybe with a core dump if enabled in the environment
node has its own memory limits
as long as that's low enough, you'll hit those before the system runs out of memory and the process gets killed
linux tends to never actually fail an _allocation_, and instead only actually allocate memory when you access it
and then if it can't, and it can't free up memory elsewhere, it just kills a process
but usually by this point everything on the system has slowed to a crawl, so you've already lost
don't think there's a way to specify memory limits for rust in particular?
https://www.kernel.org/doc/gorman/html/understand/understand016.html talks about the kernel's general behavior
you could make a global allocator with a memory limit
but that's not really going to be all that helpful until oom->unwind is a thing you can do
there's various system-specific ways to do it with various issues (ulimit -m can do _some_ things, there's memory cgroups)
	ulimit -m will do that
