# Public repository for YAML DoS vulnerabilities study

Study to determine the extent to which YAML parsers are vulnerable to billion-laughs/recursion style attacks.

## Installation
You will need Docker installed, to run the tests clone this repository to your machine.

## How to run tests:
```
cd docker/
./run.sh
cd src
./test-runner.sh

```
The results are saved in ./out/log.txt 
The name of the executable (e.g. node ./js/main.js)
Followed by the payload (e.g. loop.yaml) and the process exit code. If the process times out the exit code is 124.

## Directory structure
```
./src     # Parser drivers for  various languages
./src/payloads # YAML files for the tests
./src/test-runner.sh  # Script to execute all parser drivers specified in ./src/libraries.txt with payloads listed in ./src/payloads.txt
./docker  # Docker configuration and bash scripts
./out     # Directory for output file, log.txt
```

You can add additional YAML files in the payloads directory and list them in payloads.txt for inclusion in the tests.
