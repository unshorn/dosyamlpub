#!/bin/bash
OUT_DIR=$(dirname `pwd`)/out
cp -r ../src .
if [ $? -ne 0 ]
then
  echo "Error copying code to docker directory"
  exit 1
fi
docker build  --shm-size=1gb -t yamlstudy . 
if [ $? -eq 0 ]
then
    docker run -it --cap-add=SYS_PTRACE --security-opt seccomp=unconfined --shm-size=1gb -v $OUT_DIR:/out --env-file env.list yamlstudy bash
else
    echo "Docker build failed!" >&2
fi

